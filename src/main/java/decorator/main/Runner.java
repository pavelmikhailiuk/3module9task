package decorator.main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import decorator.domain.Person;
import decorator.stream.PersonInputStream;
import decorator.stream.PersonOutputStream;
import decorator.stream.impl.PersonInputStreamImpl;
import decorator.stream.impl.PersonOutputStreamImpl;

public class Runner {
	public static void main(String[] args) {
		File file = new File("persons.txt");
		PersonInputStream pis = null;
		PersonOutputStream pos = null;
		try {
			pos = new PersonOutputStreamImpl(new FileOutputStream(file));
			Person person = new Person("pavel");
			Person person1 = new Person("alex");
			pos.writePerson(person);
			pos.writePerson(person1);
		} catch (Exception e) {
			System.err.println(e);
		} finally {
			if (pos != null) {
				try {
					pos.close();
				} catch (IOException e) {
					System.err.println(e);
				}
			}
		}
		try {
			pis = new PersonInputStreamImpl(new FileInputStream(file));
			List<Person> persons = pis.readPersons();
			System.err.println(persons);
		} catch (Exception e) {
			System.err.println(e);
		} finally {
			if (pis != null) {
				try {
					pis.close();
				} catch (IOException e) {
					System.err.println(e);
				}
			}
		}
	}
}
