package decorator.stream;

import java.io.IOException;

import decorator.domain.Person;

public interface PersonOutputStream {
	void writePerson(Person person) throws IOException;

	void close() throws IOException;
}
