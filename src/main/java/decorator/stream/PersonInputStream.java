package decorator.stream;

import java.io.IOException;
import java.util.List;

import decorator.domain.Person;

public interface PersonInputStream {
	List<Person> readPersons() throws IOException;

	void close() throws IOException;
}
