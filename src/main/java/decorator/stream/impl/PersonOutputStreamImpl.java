package decorator.stream.impl;

import java.io.IOException;
import java.io.OutputStream;

import decorator.domain.Person;
import decorator.stream.PersonOutputStream;

public class PersonOutputStreamImpl implements PersonOutputStream {

	private OutputStream outputStream;

	public PersonOutputStreamImpl(OutputStream outputStream) {
		this.outputStream = outputStream;
	}

	public void writePerson(Person person) throws IOException {
		String name = person.getName();
		name = Character.toUpperCase(name.charAt(0)) + name.substring(1);
		outputStream.write(name.getBytes());
		outputStream.write(",".getBytes());
	}

	public void close() throws IOException {
		outputStream.close();
	}
}
