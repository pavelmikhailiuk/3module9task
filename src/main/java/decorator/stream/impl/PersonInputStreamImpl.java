package decorator.stream.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import decorator.domain.Person;
import decorator.stream.PersonInputStream;

public class PersonInputStreamImpl implements PersonInputStream {

	private InputStream inputStream;

	public PersonInputStreamImpl(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public List<Person> readPersons() throws IOException {
		StringBuilder sb = new StringBuilder();
		int i = 0;
		while ((i = inputStream.read()) != -1) {
			char c = (char) i;
			sb.append(c);
		}
		return initPersons(sb.toString());
	}

	public void close() throws IOException {
		inputStream.close();
	}

	private List<Person> initPersons(String personData) {
		String[] persons = personData.split(",");
		List<Person> personList = new ArrayList<Person>();
		for (int i = 0; i < persons.length; i++) {
			String name = persons[i];
			name = Character.toUpperCase(name.charAt(0)) + name.substring(1);
			personList.add(new Person(name));
		}
		return personList;
	}
}
